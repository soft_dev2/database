/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.yanika.databaseproject;

import com.yanika.databaseproject.model.User;
import com.yanika.databaseproject.service.UserService;

/**
 *
 * @author ASUS
 */
public class TestUserService {
  
    public static void main(String[] args) {
        UserService userService = new UserService();
        User user = userService.login("user2", "password2");
        if (user != null) {
            System.out.println("Wellcome user : " + user.getName());
        }else{
                        System.out.println("Error");
        }
    }
}
