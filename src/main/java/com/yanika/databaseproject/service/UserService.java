/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.yanika.databaseproject.service;

import com.yanika.databaseproject.dao.UserDao;
import com.yanika.databaseproject.model.User;

/**
 *
 * @author ASUS
 */
public class UserService {
        public User login(String name , String password){
           UserDao userDao = new UserDao();
           User user = userDao.getByName(name);
            if(user != null && user.getPassword().equals(password)){
                return user ;
            }
            return null ;
        }
}
